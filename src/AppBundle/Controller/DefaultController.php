<?php

namespace AppBundle\Controller;

use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $kernel = $this->container->get('kernel');
        $process = new Process($kernel->getRootDir().'/console list app --format json');

        $application = new Application($kernel);

        $output = array();
        try {
            $process->mustRun();

            $output = json_decode($process->getOutput(), true);
        } catch (ProcessFailedException $e) {
            echo $e->getMessage();
        }

        $commands = array();

        foreach ($output['commands'] as $command) {
            $commands[$command['name']] = $command;
            get_class($application->find($command['name']));
        }

        exit;

        return array('commands' => $output);
    }

    /**
     * @Route("/execute/{command}")
     */
    public function executeCommandAction($command)
    {

        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => $command
        ));

        $output = new BufferedOutput(
            OutputInterface::VERBOSITY_NORMAL,
            true
        );

        $application->run($input, $output);

        $converter = new AnsiToHtmlConverter();
        $content = $output->fetch();

        return new Response($converter->convert($content));
    }

    /**
     * @Route("/list-commands")
     * @Template()
     */
    public function listCommandsAction()
    {
        $kernel = $this->container->get('kernel');
        $process = new Process($kernel->getRootDir().'/console list app --format json');

        $output = array();
        try {
            $process->mustRun();

            $output = json_decode($process->getOutput(), true);
        } catch (ProcessFailedException $e) {
            echo $e->getMessage();
        }

        return array('commands' => $output);
    }
}
